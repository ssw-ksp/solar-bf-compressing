id: h5_to_fits
label: h5_to_fits
class: CommandLineTool
cwlVersion: v1.2
inputs: 
  - id: beamformed_file
    type: File
    secondaryFiles: 
       - pattern: '$(self.nameroot).raw'
         required: True

  - id: output_folder
    type: string
    default: result
    inputBinding:
      position: 1

  - id: chop_off
    type: boolean
    default: True
    inputBinding:
      prefix: --chop_off
      position: 2
  - id: t_c_ratio
    type: int
    default: 24
    inputBinding:
      separate: true
      prefix: --t_c_ratio
      position: 3
      
  - id: f_c_ratio
    type: int
    default: 8
    inputBinding:
      separate: true
      prefix: --f_c_ratio
      position: 4
      
  - id: minutes_per_chunk
    type: int
    default: 15
    inputBinding:
      separate: true
      prefix: --minutes_per_chunk
      position: 5
      
  - id: averaging
    type: boolean
    default: True
    inputBinding:
      prefix: --averaging
      position: 6
      
  - id: flagging
    type: boolean
    default: True
    inputBinding:
      prefix: --flagging
      position: 7
      
  - id: t_idx_cut
    type: int
    default: 128
    inputBinding:
      separate: true
      prefix: --t_idx_cut
      position: 8
      
  - id: target_directory
    type: boolean
    default: false
    inputBinding:
      prefix: --target_directory
      position: 9
      
  - id: date_directory
    type: boolean
    default: false
    inputBinding:
      prefix: --date_directory
      position: 10
      
  - id: simple_fname
    type: boolean
    default: false
    inputBinding:
      prefix: --simple_fname
      position: 11    
  
outputs: 
  - id: json_files
    type: File[]
    outputBinding:
      glob: result/*.json
  - id: inspection_png
    type: File[]
    outputBinding:
      glob: result/*.png
  - id: dynamic_spectra
    type: File[]
    outputBinding:
      glob: result/*.fits
  - id: outfolder
    type: Directory
    outputBinding:
      glob: $(inputs.output_folder)

baseCommand:
    - sh
    - script.sh

requirements:
  - class: InlineJavascriptRequirement
  - class: NetworkAccess
    networkAccess: true
hints:
  - class: DockerRequirement
    dockerPull: git.astron.nl:5000/ssw-ksp/solar-bf-compressing
  
  - class: InitialWorkDirRequirement
    listing:
      - entryname: 'script.sh' 
        entry: |
          #!/bin/bash

          h5_to_fits $(inputs.beamformed_file.basename) $@
      - entry: $(inputs.beamformed_file)