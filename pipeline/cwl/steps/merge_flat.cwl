#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: ExpressionTool

requirements:
- class: InlineJavascriptRequirement

inputs:
- id: nested_file_list
  type:
    type: array
    items:
    - 'null'
    - type: array
      items:
      - File

outputs:
- id: flattened_file_list
  type: File[]
expression: |
  ${
    var output_list = [];
    var i;
    for(i=0; i<inputs.nested_file_list.length; i++){
       output_list.push.apply(output_list, inputs.nested_file_list[i])
    }
    return { 'flattened_file_list': output_list}
  }
