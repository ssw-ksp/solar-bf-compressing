class: 'Workflow'
cwlVersion: v1.2
id: beamformed_compression
inputs: 
  - id: surls
    type: string[]

outputs: 
  - id: json_files1d
    type: 
      type: array
      items:
        type: array
        items: File
    outputSource:
      - process1d/json_files

  - id: inspection_png1d
    type: 
      type: array
      items:
        type: array
        items: File
    outputSource:
      - process1d/inspection_png

  - id: dynamic_spectra1d
    type: 
      type: array
      items:
        type: array
        items: File
    outputSource:
      - process1d/dynamic_spectra

  - id: json_files15min
    type: 
      type: array
      items:
        type: array
        items: File
    outputSource:
      - process15min/json_files

  - id: inspection_png15min
    type: 
      type: array
      items:
        type: array
        items: File
    outputSource:
      - process15min/inspection_png

  - id: dynamic_spectra15min
    type: 
      type: array
      items:
        type: array
        items: File
    outputSource:
      - process15min/dynamic_spectra

steps: 
  - id: fetch_data
    in: 
      - id: surl_link
        source: surls
    scatter: surl_link    
    out: 
       - id: beamformed_file
    run: ../steps/fetch_data.cwl

  - id: process1d
    in: 
      - id: beamformed_file
        source: fetch_data/beamformed_file
      - id: output_folder
        valueFrom: "result/data1d"
      - id: t_c_ratio
        valueFrom: "960"
      - id: minutes_per_chunk 
        valueFrom: "999"
      - id: t_idx_cut
        valueFrom: "8"
      - id: target_directory
        valueFrom: "True"
      - id: simple_fname 
        valueFrom: "True"
      - id: flagging
        valueFrom: "True"
        
    out: 
      - id: json_files
      - id: inspection_png
      - id: dynamic_spectra
      - id: outfolder

    scatter: beamformed_file
    run: ../steps/h5_to_fits.cwl

    
  - id: process15min
    in: 
      - id: beamformed_file
        source: fetch_data/beamformed_file     
      - id: output_folder
        valueFrom: "result/data15min"
      - id: t_c_ratio
        valueFrom: "48"
      - id: target_directory
        valueFrom: "True"
      - id: date_directory
        valueFrom: "True"
      - id: flagging
        valueFrom: "True"
    out: 
      - id: json_files
      - id: inspection_png
      - id: dynamic_spectra
      - id: outfolder

    scatter: beamformed_file
    run: ../steps/h5_to_fits.cwl
requirements:
  - class: ScatterFeatureRequirement