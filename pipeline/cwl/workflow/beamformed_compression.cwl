#!/usr/bin/env cwl-runner

cwlVersion: v1.2
class: Workflow

requirements:
- class: ScatterFeatureRequirement

inputs:
- id: surls
  type: string[]

outputs:
- id: json_files
  type: File
  outputSource:
  - compress_json/compressed
- id: inspection_png
  type: File
  outputSource:
  - compress_previews/compressed
- id: dynamic_spectra
  type: File
  outputSource:
  - compress_dynspec/compressed

steps:
- id: fetch_data
  in:
  - id: surl_link
    source: surls
  scatter: surl_link
  run: ../steps/fetch_data.cwl
  out:
  - id: beamformed_file
- id: process
  in:
  - id: beamformed_file
    source: fetch_data/beamformed_file
  scatter: beamformed_file
  run: ../steps/h5_to_fits.cwl
  out:
  - id: json_files
  - id: inspection_png
  - id: dynamic_spectra
- id: flatten_json_files
  in:
  - id: nested_file_list
    source: process/json_files
  run: ../steps/merge_flat.cwl
  out:
  - flattened_file_list
- id: flatten_dynspec_files
  in:
  - id: nested_file_list
    source: process/dynamic_spectra
  run: ../steps/merge_flat.cwl
  out:
  - flattened_file_list
- id: flatten_previews_files
  in:
  - id: nested_file_list
    source: process/inspection_png
  run: ../steps/merge_flat.cwl
  out:
  - flattened_file_list
- id: compress_json
  in:
  - id: files
    source: flatten_json_files/flattened_file_list
  - id: type
    valueFrom: metadata
  run: ../steps/compress.cwl
  out:
  - compressed
- id: compress_dynspec
  in:
  - id: files
    source: flatten_dynspec_files/flattened_file_list
  - id: type
    valueFrom: dynspec
  run: ../steps/compress.cwl
  out:
  - compressed
- id: compress_previews
  in:
  - id: files
    source: flatten_previews_files/flattened_file_list
  - id: type
    valueFrom: previews
  run: ../steps/compress.cwl
  out:
  - compressed
id: beamformed_compression
