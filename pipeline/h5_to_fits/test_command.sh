# -------------------
# default: 
# time downsample ratio 96,  time resolution 1 sec
# duration per chunk 15min, 
# frequency ratio 8, 
#--------------------

# with default parameters: 1s t-res, 15min chunk, 600 channels
python h5_to_fits "D:\H5\L860566_SAP000_B000_S0_P000_bf.h5" 'out' --averaging --flagging

# only first 2 chunk
python h5_to_fits "D:\H5\L860566_SAP000_B000_S0_P000_bf.h5" 'out1' --averaging --flagging --num_chunks 2

# single file summary, time resolution 10 sec
python h5_to_fits "D:\H5\L860566_SAP000_B000_S0_P000_bf.h5" 'out4' --averaging --flagging --t_c_ratio 960 --minutes_per_chunk 999 --t_idx_cut 8
