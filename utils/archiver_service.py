import configparser
import json
import logging
import os
import subprocess
import tempfile
from argparse import ArgumentParser
from datetime import datetime
from functools import reduce
from glob import glob

import requests
from apscheduler.executors.pool import ThreadPoolExecutor
from apscheduler.schedulers.blocking import BlockingScheduler
from pymongo import MongoClient

logging.basicConfig(format='%(asctime)s - %(levelname)s: %(message)s', level=logging.DEBUG)
CONFIGFILE_PATH = ''


def normalize_nested_list(nested_list):
    if isinstance(nested_list, list):
        return reduce(lambda a, b: a + normalize_nested_list(b), nested_list, [])
    else:
        return [nested_list]


def parse_args():
    parser = ArgumentParser(description='Archiver service for solar data')
    parser.add_argument('config')
    return parser.parse_args()


def get_surls_per_task(task_obj):
    outputs = task_obj['outputs']
    surls_per_key = {}
    for key in outputs:
        if isinstance(outputs[key], list):
            items = normalize_nested_list(outputs[key])
        
            if len(items) == 1:
                items, *_ = items
            files_in_key = [item['surl'] for item in items if item and ('surl' in item)]
        elif "surl" in outputs[key]:
            files_in_key = [outputs[key]["surl"]]
        else:
            continue
        surls_per_key[key] = files_in_key
    return surls_per_key


def task_storage_dir_slug(storage_path, workflow, sas_id, task_id):
    return os.path.join(storage_path, workflow, f'{sas_id}', f'{task_id}')


def key_storage_dir_slug(storage_path, workflow, sas_id, task_id, item_type):
    return os.path.join(task_storage_dir_slug(storage_path, workflow, sas_id, task_id), item_type)


def filename_from_surl(surl):
    return surl.split('/')[-1]


def _surl_to_gftp(surl):
    return surl.replace('srm://srm.grid.sara.nl/', 'gsiftp://gridftp.grid.sara.nl/')


class Archiver:
    def __init__(self,
                 storage_path,
                 container_name,
                 grid_pass,
                 proxy_path,
                 proxy_server,
                 storage_db):
        self.storage_path = storage_path
        self.container_name = container_name
        self.grid_pass = grid_pass
        self.proxy_path = proxy_path
        self.proxy_server = proxy_server
        self.storage_db = storage_db

    def renew_proxy(self):
        proxy_dir = os.path.dirname(self.proxy_path)
        cmd = f'udocker run' \
              f' -v {proxy_dir}:{proxy_dir} {self.container_name}' \
              f' myproxy-logon -S -s {self.proxy_server}' \
              f' -l "c=eu,o=lofar,ou=services,cn=proxy_ldv" -t 167 -o {self.proxy_path}'

        result = subprocess.run(cmd, stdout=subprocess.PIPE, shell=True, input=f'{self.grid_pass}\n'.encode())
        logging.info('Proxy certificate renewed')
        try:
            result.check_returncode()
            return result.returncode
        except subprocess.CalledProcessError as e:
            logging.error('An error occurred: %s', e)
            logging.error(result.stdout)
            logging.error(result.stderr)
            return -1

    def download_url_list(self, url_list, storage_path):
        HOME = os.path.expanduser('~')
        with tempfile.NamedTemporaryFile(mode='r+') as f_stream:
            for line in url_list:
                f_stream.write(f'{line}\n')
            f_stream.flush()
            cmd = f'udocker run -e X509_USER_PROXY=/globus/proxy.cert -v {HOME}/.globus:/globus' \
                  f' -v {storage_path}:{storage_path} -v {f_stream.name}:{f_stream.name} {self.container_name}' \
                  f' globus-url-copy -f {f_stream.name}'
                  

            download = subprocess.run(cmd, stdout=subprocess.PIPE, shell=True)
            try:
                download.check_returncode()
            except subprocess.CalledProcessError as e:
                logging.error('An error occurred: %s', e)
                logging.error(download.stdout)
                logging.error(download.stderr)
                logging.error('Error copying files to %s: \n %s', storage_path, url_list)
                return -1

            decompress = 'find . -name "*.tar" -exec tar -xvf  {} --strip-components=1 \;'
            decompress = subprocess.run(decompress, stdout=subprocess.PIPE, shell=True, 
                                        cwd=storage_path)
            
            try:
                decompress.check_returncode()
            except subprocess.CalledProcessError as e:
                logging.error('An error occurred: %s', e)
                logging.error(decompress.stdout)
                logging.error(decompress.stderr)
                logging.error('Error copying files to %s: \n %s', storage_path, url_list)
                return -1
            
            clean_up = 'find . -name "*.tar" -exec rm {} \;'
            clean_up = subprocess.run(clean_up, stdout=subprocess.PIPE, shell=True, 
                                        cwd=storage_path)
            try:
                clean_up.check_return_code()
            except subprocess.CalledProcessError as e:
                logging.error('An error occurred: %s', e)
                logging.error(clean_up.stdout)
                logging.error(clean_up.stderr)
                logging.error('Error copying files to %s: \n %s', storage_path, url_list)
                return -1
            
            return 0
        
    @staticmethod
    def load_from_config(configfile_path):
        parser = configparser.ConfigParser()
        parser.read(configfile_path)
        return Archiver(parser['ARCHIVER']['storage_path'],
                        parser['ARCHIVER']['container_name'],
                        parser['ARCHIVER']['grid_password'],
                        parser['ARCHIVER']['proxy_path'],
                        parser['ARCHIVER']['proxy_server'],
                        parser['ARCHIVER']['storage_db'])

    def connect_to_database(self):
        client = MongoClient('mongodb://localhost')
        return client[self.storage_db]

    def collection_per_workflow(self, workflow):
        workflow_collection = self.connect_to_database()[workflow]
        workflow_collection.create_index((('storage_path', 1), ('filename', 1)))
        return workflow_collection

    def store_task_dataproduct(self, sas_id, task_id, workflow, data_surls):

        for key in data_surls:
            storage_dir = key_storage_dir_slug(self.storage_path, workflow, sas_id, task_id, key)
            os.makedirs(storage_dir, exist_ok=True)
            url_list = []

            for surl in data_surls[key]:
                filename = filename_from_surl(surl)
                output_file_path = 'file://' + os.path.join(storage_dir, filename)
                gridftp = _surl_to_gftp(surl)
                url_list.append(' '.join((gridftp, output_file_path)))
            if url_list:
                return_code = self.download_url_list(url_list, storage_dir)

                if return_code < 0:
                    raise Exception('error in storing dataset')
            else:
                logging.info('nothing to download for task %s - %s', task_id, key)

        task_storage_dir = task_storage_dir_slug(self.storage_path, workflow, sas_id, task_id)
        metadata_files = glob(os.path.join(task_storage_dir, '**/*.json'), recursive=True)
        logging.debug('Found the following metadata files: %s', metadata_files)
        workflow_collection = self.collection_per_workflow(workflow)
        for file_path in metadata_files:
            with open(file_path, 'r') as fin:
                metaobj = json.load(fin)
                metaobj['filename'] = file_path
                metaobj['storage_path'] = task_storage_dir
                workflow_collection.replace_one({'filename': file_path, 'storage_path': task_storage_dir},
                                                metaobj, upsert=True)


class ATDB:
    def __init__(self, url, token):
        self.token = token
        self.url = url

    @staticmethod
    def load_from_config(configfile_path):
        parser = configparser.ConfigParser()
        parser.read(configfile_path)
        return ATDB(parser['ATDB']['url'], parser['ATDB']['token'])

    def get_stored_tasks(self):
        full_url = self.url + '/tasks'
        response = requests.get(full_url, params={'status': 'stored'})
        tasks = response.json()['results']
        return tasks

    def get_scrubbed_tasks(self):
        full_url = self.url + '/tasks'
        response = requests.get(full_url, params={'status': 'scrubbed'})
        tasks = response.json()['results']
        return tasks

    def get_workflow(self, workflow_id):
        full_url = self.url + f'/workflows/{workflow_id}'
        response = requests.get(full_url)
        return response.json()

    def get_session(self):
        session = requests.session()
        session.headers['Authorization'] = f'Token {self.token}'
        session.headers['content-type'] = 'application/json'
        session.headers['cache-control'] = 'no-cache'
        return session

    def _log_entry(self, task_id, status):
        full_url = self.url + f'/logentries/'
        timestamp = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
        payload = dict(task=task_id, status=status, step_name='archive', service='archiver',
                       timestamp=timestamp)
        response = self.get_session().post(full_url, json=payload)
        if not response.ok:
            logging.error('Error contacting ATDB %s: %s - %s', response.status_code, response.reason, response.content)

    def set_task_to_archived(self, task_id):
        self._log_entry(task_id, 'archived')

    def set_task_to_archived_failed(self, task_id):
        self._log_entry(task_id, 'archived_failed')

    def set_task_to_stored(self, task_id):
        self._log_entry(task_id, 'stored')

    def set_task_to_validated(self, task_id):
        self._log_entry(task_id, 'validated')


scheduler = BlockingScheduler(executor={'default': ThreadPoolExecutor})


def check_iteration(configfile_path):
    atdb = ATDB.load_from_config(configfile_path)
    archiver = Archiver.load_from_config(configfile_path)
    tasks = atdb.get_stored_tasks()
    for task in tasks:
        task_id = task['id']
        atdb.set_task_to_validated(task_id)

    tasks = atdb.get_scrubbed_tasks()

    for task in tasks:
        task_id = task['id']
        sas_id = task['sas_id']
        workflow_name = atdb.get_workflow(task['workflow'])['workflow_uri']

        try:
            surls = get_surls_per_task(task)
            archiver.store_task_dataproduct(task_id, sas_id, workflow_name, surls)
            logging.info('Storing task %s', task_id)
            atdb.set_task_to_archived(task_id)
        except Exception as e:
            logging.exception(e)
            logging.error(f'Error archiving task {task_id}')
            atdb.set_task_to_archived_failed(task_id)


def renew_proxy(configfile_path):
    archiver = Archiver.load_from_config(configfile_path)
    archiver.renew_proxy()


def main():
    args = parse_args()
    renew_proxy(args.config)
    check_iteration(args.config)
    scheduler.add_job(check_iteration, 'interval', minutes=2, args=(args.config,))
    scheduler.add_job(renew_proxy, 'interval', minutes=1, args=(args.config,))
    scheduler.start()


if __name__ == '__main__':
    main()
