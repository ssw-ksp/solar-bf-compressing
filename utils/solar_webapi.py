import logging

import fastapi.exceptions
from fastapi import FastAPI
from typing import Optional
import os
from datetime import datetime, date
import numpy
from fastapi.responses import StreamingResponse
import png
from io import BytesIO
from pymongo import MongoClient
from pymongo.collection import Collection
from configparser import ConfigParser
from typing import Union
from astropy.io import fits
from skimage.transform import resize as im_resize

solar_config_path = os.environ.get('SOLAR_CONFIG')
solar_dynspec_xsize = os.environ.get('SOLAR_DYNSPEC_XSIZE', 400)
solar_dynspec_ysize = os.environ.get('SOLAR_DYNSPEC_YSIZE', 400)


def rescale(value, min_value, max_value):
    return ((value - min_value) / (max_value - min_value) * 255).astype('uint8')


def fromisoformat(date_time_str):
    ISOFORMAT = '%Y-%m-%d %H:%M:%S.%f'
    return datetime.strptime(date_time_str, ISOFORMAT)


def fromisoformat_date(date_time_str):
    DATE_ISOFORMAT = '%Y-%m-%d'
    return datetime.strptime(date_time_str, DATE_ISOFORMAT)


def generate_png(data, x_pixels=solar_dynspec_xsize, y_pixels=solar_dynspec_ysize, v_min=None, v_max=None):
    if v_min == None:
        v_min = numpy.min(data)
    if v_max == None:
        v_max = numpy.max(data)
    data = im_resize(data, (x_pixels, y_pixels))
    data = rescale(data.T[::-1, ::1], v_min, v_max)
    buffer = BytesIO()
    w = png.Writer(y_pixels, x_pixels)
    w.write(buffer, data.tolist())
    buffer.seek(0)
    return buffer.getvalue()


def get_fits_path_from_task(task):
    basename = os.path.basename(task['filename']).split('.')[0] + '.fits'
    storage_path = task['storage_path']
    fits_file_path = os.path.join(storage_path, 'dynamic_spectra', basename)
    return fits_file_path


def get_png_path_from_task(task):
    basename = os.path.basename(task['filename']).split('.')[0] + '.png'
    storage_path = task['storage_path']
    png_file_path = os.path.join(storage_path, 'inspection_png', basename)
    return png_file_path


def augument_task(task):
    task['png'] = get_png_path_from_task(task)
    task['fits'] = get_fits_path_from_task(task)
    task['beam'] = extract_beam_from_source(task['source'])
    return task


def resample_spectrum(x, y, sample):
    resampled_x_axis = numpy.linspace(numpy.min(x), numpy.max(x), sample, endpoint=True)
    resampled_y_axis = numpy.interp(resampled_x_axis, x, y)
    return resampled_x_axis, resampled_y_axis


def extract_beam_from_source(source_fname):
    return source_fname[16: 19]


class DataProductDB:
    def __init__(self):
        self.db: Union[Collection, None] = None
        self.cache = {}
        self.storage_db = ''
        self.configure()
        self.connect_db()

    def configure(self):
        parser = ConfigParser()
        parser.read(solar_config_path)
        self.storage_db = parser['ARCHIVER']['storage_db']

    def connect_db(self):
        self.db: Collection = MongoClient('mongodb://localhost')[self.storage_db]['solar_bf_compressing']

    def available_dates(self):
        return self.db.distinct('date')

    def available_beams(self, date_str):
        if date_str:
            return sorted(
                {extract_beam_from_source(fname) for fname in self.db.find({'date': date_str}).distinct('source')})
        else:
            return sorted({extract_beam_from_source(fname) for fname in self.db.find().distinct('source')})

    def list(self, date_str=None, beam=None):
        query = {}
        if date_str is not None:
            query['date'] = date_str
        if beam is not None:
            query['source'] = {'$regex': f'B{beam:03d}'}
        return [augument_task(item) for item in self.db.find(query, {'_id': 0})]

    def available_beam_positions(self, date_str=None):
        return [item for item in self.db.find({'date': date_str},
                                              {'x': 1, 'y': 1, 'ra': 1, 'dec': 1, 'time': 1, '_id': 0})]

    def get_dataset(self, obs_id, time):
        task = self.db.find_one({'obsID': str(obs_id), 'time': str(time)})
        if task is None:
            return None

        fits_file_path = get_fits_path_from_task(task)

        with fits.open(fits_file_path) as fin:
            data = fin[0].data
            data -= numpy.tile(numpy.mean(data, 0), (data.shape[0], 1))
            mean = numpy.mean(data)
            std = numpy.std(data)
            return data, mean, std

    def get_plot(self, obs_id, time):
        data, mean, std = self.get_dataset(obs_id, time)
        png = generate_png(data, v_min=mean - 2 * std, v_max=mean + 3 * std)
        return png

    def _get_time(self, obs_id, time):
        task = self.db.find_one({'obsID': str(obs_id), 'time': str(time)})
        fits_file_path = get_fits_path_from_task(task)
        with fits.open(fits_file_path) as hdus:
            time_axis = hdus[1].data[0]['TIME']
            return time_axis

    def get_time(self, obs_id, time):
        time_axis = self._get_time(obs_id, time)
        return numpy.linspace(numpy.min(time_axis), numpy.max(time_axis), solar_dynspec_xsize).tolist()

    def _get_frequency(self, obs_id, time):
        task = self.db.find_one({'obsID': str(obs_id), 'time': str(time)})
        fits_file_path = get_fits_path_from_task(task)
        with fits.open(fits_file_path) as hdus:
            freq_axis = hdus[1].data[0]['FREQ']
            return freq_axis

    def get_frequency(self, obs_id, time):
        freq_axis = self._get_frequency(obs_id, time)
        return numpy.linspace(numpy.min(freq_axis), numpy.max(freq_axis), solar_dynspec_ysize).tolist()

    def get_spectrum(self, obs_id, time, sample):
        data, mean, std = self.get_dataset(obs_id, time)

        intensity = data[int(sample), :].tolist()

        frequency = self._get_frequency(obs_id, time)
        frequency, intensity = resample_spectrum(frequency, intensity, solar_dynspec_ysize)
        return [{'x': f_i, 'y': I_i} for f_i, I_i in zip(frequency, intensity)]

    def get_time_evol(self, obs_id, time, sample):
        data, mean, std = self.get_dataset(obs_id, time)
        intensity = data[:, int(sample)].tolist()
        time = self._get_time(obs_id, time)
        time, intensity = resample_spectrum(time, intensity, solar_dynspec_xsize)
        return [{'x': f_i, 'y': I_i} for f_i, I_i in zip(time, intensity)]


DB = DataProductDB()
app = FastAPI()


def validate_date(date_str):
    date_items = date_str.split('-')
    if len(date_items) != 3:
        raise ValueError('')


@app.get('/tasks')
def read_tasks(date: Optional[str] = None, beam: Optional[int] = None):
    return DB.list(date, beam=beam)


@app.get('/beams')
def read_beams(date: Optional[str] = None):
    return DB.available_beams(date)


@app.get('/dates')
def read_dates():
    return DB.available_dates()


@app.get('/beam_position')
def read_beam_position(date: Optional[str] = None):
    return DB.available_beam_positions(date)


@app.get('/dynspec/{obs_id}/{time}/image')
async def read_dynspec_image(obs_id: str, time: str):
    return StreamingResponse(iter([DB.get_plot(obs_id, time)]), media_type='image/png')


@app.get('/dynspec/{obs_id}/{time}/time')
async def read_dynspec_time(obs_id: str, time: str):
    return DB.get_time(obs_id, time)


@app.get('/dynspec/{obs_id}/{time}/frequency')
async def read_dynspec_frequency(obs_id: str, time: str):
    return DB.get_frequency(obs_id, time)


@app.get('/dynspec/{obs_id}/{time}/spectrum/{sample}/')
async def read_dynspec_frequency(obs_id: str, time: str, sample: str):
    return DB.get_spectrum(obs_id, time, sample)


@app.get('/dynspec/{obs_id}/{time}/timeseries/{sample}/')
async def read_dynspec_frequency(obs_id: str, time: str, sample: str):
    return DB.get_time_evol(obs_id, time, sample)
