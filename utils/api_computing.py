import logging

from fastapi import FastAPI
from typing import Optional
import os
import json
from functools import reduce
import pandas
from datetime import datetime, date
import numpy
from fastapi.responses import StreamingResponse

import re

solar_path = os.environ.get('SOLAR_DB_DIR')
solar_dynspec_xsize = os.environ.get('SOLAR_DYNSPEC_XSIZE', 400)
solar_dynspec_ysize = os.environ.get('SOLAR_DYNSPEC_YSIZE', 400)

from astropy.io import fits
from skimage.transform import resize as im_resize


def rescale(value, min_value, max_value):
    return ((value - min_value) / (max_value - min_value) * 255).astype('uint8')


import png
from io import BytesIO


def fromisoformat(date_time_str):
    ISOFORMAT = '%Y-%m-%d %H:%M:%S.%f'
    return datetime.strptime(date_time_str, ISOFORMAT)


def fromisoformat_date(date_time_str):
    DATE_ISOFORMAT = '%Y-%m-%d'
    return datetime.strptime(date_time_str, DATE_ISOFORMAT)


def generate_png(data, x_pixels=solar_dynspec_xsize, y_pixels=solar_dynspec_ysize, v_min=None, v_max=None):
    if v_min == None:
        v_min = numpy.min(data)
    if v_max == None:
        v_max = numpy.max(data)
    data = im_resize(data, (x_pixels, y_pixels))
    data = rescale(data.T[::-1, ::1], v_min, v_max)
    buffer = BytesIO()
    w = png.Writer(y_pixels, x_pixels)
    w.write(buffer, data.tolist())
    buffer.seek(0)
    return buffer.getvalue()


def preparse_db(item):
    beam_regex = r'.*B([0-9]{3}).*'
    beam_number = re.search(beam_regex, item['source'])[1]
    item['date'] = fromisoformat_date(item['date'])
    item['time_range'] = fromisoformat(item['time_range'][0]), fromisoformat(item['time_range'][1])
    item['beam'] = int(beam_number)
    return item


class DataProductDB:
    def __init__(self):
        self.db = {}
        self.cache = {}
        self._load_db()
        self.sort_db()
        self._prepare_summaries()

    def _load_db(self):
        with open(os.path.join(solar_path, 'summary.json'), 'r') as db_stream:
            tasks = json.load(db_stream)
            tasks_meta = map(preparse_db, reduce(lambda x, y: x + list(y.values()), tasks.values(), []))

            self.db: pandas.DataFrame = pandas.DataFrame(tasks_meta)

    def _prepare_summaries(self):
        unique_obs_ids = self.db['obsID'].unique()
        main_path = os.path.join(solar_path, 'cache')
        os.makedirs(main_path, exist_ok=True)
        for obs_id in unique_obs_ids:
            dset = self.db[self.db['obsID'] == obs_id]
            self.cache[obs_id] = {}
            unique_times = dset['time'].unique()
            for time in unique_times:

                f_out_name = f'{obs_id}-{time}.npz'
                f_out_full = os.path.join(main_path, f_out_name)
                self.cache[obs_id][time] = f_out_full
                if os.path.exists(f_out_full):
                    continue
                fits_paths = dset[dset['time'] == time].sort_values('beam')['fits'].tolist()

                try:
                    first_fits = fits.open(os.path.join(solar_path, fits_paths[0]))
                    freq, time = first_fits[1].data['FREQUENCY'], first_fits[1].data['TIME']
                    down_sampled = numpy.zeros((400, 400, len(fits_paths)))
                    out_array = fits.open(os.path.join(solar_path, fits_paths[0]))[0].data
                    down_sampled[:, :, 0] = im_resize(out_array, (400, 400))

                    for i, fits_path in enumerate(fits_paths[1:]):
                        data = fits.open(os.path.join(solar_path, fits_path))[0].data
                        out_array += data
                        down_sampled[:, :, i + 1] = im_resize(data, (400, 400))
                    out_array /= len(fits_paths)

                    numpy.savez(f_out_full, time=time, frequency=freq, data=out_array, downsampled=down_sampled)
                except Exception as e:
                    logging.exception(e)
                    print('an error occurred for ', obs_id, time)

    def sort_db(self):
        self.db.sort_values('date')

    def available_dates(self):
        return [str(date) for date in self.db.sort_values(['date']).date.unique()]

    def available_beams(self, date_str):
        dset = self.db
        if date_str is not None:
            dset = dset[dset['date'] == fromisoformat_date(date_str)]

        return [str(beam) for beam in dset.sort_values(['beam']).beam.unique()]

    def list(self, date_str=None, beam=None):
        dset = self.db
        if date_str is not None:
            dset = dset[dset['date'] == fromisoformat_date(date_str)]

        if beam is not None:
            dset = dset[dset['beam'] == int(beam)]

        return dset.sort_values(['time', 'source']).astype('str').to_dict('records')

    def available_beam_positions(self, date_str=None):
        dset = self.db
        dset = dset[dset['pointing_name'] == 'Sun']
        if date_str is not None:
            dset = dset[dset['date'] == date.fromisoformat(date_str)]

        return dset[['x', 'y', 'ra', 'dec', 'time']].astype(str).to_dict('records')

    def get_plot(self, obs_id, time):
        path = self.cache[obs_id][time]
        data_set = numpy.load(path, allow_pickle=True)
        data = numpy.array(data_set['data'], dtype=float)
        data -= numpy.tile(numpy.mean(data, 0), (data.shape[0], 1))
        mean = numpy.mean(data)
        std = numpy.std(data)

        png = generate_png(data, v_min=mean - 2 * std, v_max=mean + 3 * std)
        return png

    def get_time(self, obs_id, time):
        path = self.cache[obs_id][time]

        data_set = numpy.load(path, allow_pickle=True)

        t = numpy.array(data_set['time'][0], dtype=float)
        x = numpy.linspace(0, len(t), len(t))
        x_new = numpy.linspace(0, len(t), len(t))
        t = numpy.interp(x_new, x, t)
        return t.tolist()

    def get_frequency(self, obs_id, time):
        path = self.cache[obs_id][time]
        data_set = numpy.load(path, allow_pickle=True)

        f = numpy.array(data_set['frequency'][0], dtype=numpy.float)
        x = numpy.linspace(0, len(f), len(f))
        x_new = numpy.linspace(0, len(f), solar_dynspec_ysize)
        f = numpy.interp(x_new, x, f)
        return f.tolist()

    def get_spectrum(self, obs_id, time, sample):
        path = self.cache[obs_id][time]
        data_set = numpy.load(path, allow_pickle=True)
        data = numpy.array(data_set['data'], dtype=float)
        data -= numpy.tile(numpy.mean(data, 0), (data.shape[0], 1))
        data = im_resize(data, (solar_dynspec_xsize, solar_dynspec_ysize))
        frequency = self.get_frequency(obs_id, time)
        intensity = data[int(sample), :].tolist()
        return [{'x': f_i, 'y': I_i} for f_i, I_i in zip(frequency, intensity)]

    def get_time_evol(self, obs_id, time, sample):
        path = self.cache[obs_id][time]
        data_set = numpy.load(path, allow_pickle=True)
        data = numpy.array(data_set['data'], dtype=float)
        data -= numpy.tile(numpy.mean(data, 0), (data.shape[0], 1))
        data = im_resize(data, (solar_dynspec_xsize, solar_dynspec_ysize))
        time = self.get_time(obs_id, time)
        intensity = data[:, int(sample)].tolist()
        return [{'x': f_i, 'y': I_i} for f_i, I_i in zip(time, intensity)]


DB = DataProductDB()
app = FastAPI()


def validate_date(date_str):
    date_items = date_str.split('-')
    if len(date_items) != 3:
        raise ValueError('')


@app.get('/tasks')
def read_tasks(date: Optional[str] = None, beam: Optional[int] = None):
    return DB.list(date, beam=beam)


@app.get('/beams')
def read_beams(date: Optional[str] = None):
    return DB.available_beams(date)


@app.get('/dates')
def read_dates():
    return DB.available_dates()


@app.get('/beam_position')
def read_beam_position(date: Optional[str] = None):
    return DB.available_beam_positions(date)


@app.get('/dynspec/{obs_id}/{time}/image')
async def read_dynspec_image(obs_id: str, time: str):
    return StreamingResponse(iter([DB.get_plot(obs_id, time)]), media_type='image/png')


@app.get('/dynspec/{obs_id}/{time}/time')
async def read_dynspec_time(obs_id: str, time: str):
    return DB.get_time(obs_id, time)


@app.get('/dynspec/{obs_id}/{time}/frequency')
async def read_dynspec_frequency(obs_id: str, time: str):
    return DB.get_frequency(obs_id, time)


@app.get('/dynspec/{obs_id}/{time}/spectrum/{sample}/')
async def read_dynspec_frequency(obs_id: str, time: str, sample: str):
    return DB.get_spectrum(obs_id, time, sample)


@app.get('/dynspec/{obs_id}/{time}/timeseries/{sample}/')
async def read_dynspec_frequency(obs_id: str, time: str, sample: str):
    return DB.get_time_evol(obs_id, time, sample)
