#!/h5_to_fits/env python
from argparse import ArgumentParser
import pandas
import urllib.parse
from sqlalchemy import create_engine
from typing import Dict, Union, List, Tuple
import json
import logging
import os.path
from configparser import ConfigParser
from sunpy.coordinates.sun import sky_position as sun_position, P as precession_angle
from numpy import sin, cos, array as np_array

CONNECTION_STRING = 'oracle+cx_oracle://{user}:{password}@db.lofar.target.rug.nl/?service_name=db.lofar.target.rug.nl'
DEFAULT_CONFIG_PATHS = ['~/.config/solar_processing/config.ini', '~/.awe/Environment.cfg']


def parse_args():
    parser = ArgumentParser(description='Query the LTA to improve the content of the summary')
    parser.add_argument('summary_file', help='Excel file with the list of the observations')
    return parser.parse_args()


logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.INFO)

logger = logging.getLogger('Solar query LTA')

COLUMN_NAME = '(System) Coherent Stokes (BF data)'
QUERY_STR = '''SELECT FO.OBJECT_ID AS OBJECT_ID,
       FO.FILESIZE AS FILESIZE,
       ST.OBS_ID as OBS_ID,
       FO."+PROJECT" AS PROJECT,
       P.RIGHTASCENSION AS RA,
       P.DECLINATION AS DEC,
       PO.RIGHTASCENSION AS RA_OFFSET,
       PO.DECLINATION AS DEC_OFFSET,
       SUBSTR(FO.FILENAME, 1, Instr(FO.FILENAME, '_', -1, 1) -1) AS FILENAME,
       STR.PRIMARY_URL AS PRIMARY_URL FROM AWOPER.FILEOBJECT FO JOIN
    AWOPER.STORAGETICKETRESOURCE STR ON FO.STORAGE_TICKET_RESOURCE=STR.OBJECT_ID JOIN
    AWOPER.STORAGETICKET ST ON ST.OBJECT_ID=STR.TICKET JOIN
    AWOPER.FILEOBJECT FO ON FO.STORAGE_TICKET_RESOURCE = STR.OBJECT_ID JOIN
    AWOPER.BEAMFORMEDDATAPRODUCT BDP ON FO.DATA_OBJECT = BDP.OBJECT_ID JOIN
    AWOPER.BEAMFORMEDDATAPRODUCT$BEAMS BDPB ON BDPB.OBJECT_ID = BDP.OBJECT_ID JOIN
    AWOPER.COHERENTSTOKESBEAM AB ON BDPB.COLUMN_VALUE = AB.OBJECT_ID  JOIN
    AWOPER.POINTING P ON AB.POINTING = P.OBJECT_ID JOIN
    AWOPER.POINTING PO ON AB.OFFSET = PO.OBJECT_ID
       WHERE OBS_ID = '{}'
'''


def read_user_credentials(file_paths: Union[str, List[str]]) -> Dict:
    file_paths = list(map(os.path.expanduser, map(os.path.expandvars, file_paths)))

    parser = ConfigParser()
    parser.read(file_paths)
    credentials = {'user': parser['global']['database_user'], 'password': parser['global']['database_password']}
    return credentials


def create_db_engine(user: str, password: str):
    connection_string: str = CONNECTION_STRING.format(user=urllib.parse.quote_plus(user),
                                                      password=urllib.parse.quote_plus(password))

    return create_engine(connection_string)


def read_summary_file(path):
    with open(path, 'r') as f_stream:
        summary = json.load(f_stream)
        sas_id_to_id = {}
        for id_str in summary:
            sas_id, _ = id_str.split('-')
            if sas_id in sas_id_to_id:
                sas_id_to_id[sas_id].append(id_str)
            else:
                sas_id_to_id[sas_id] = [id_str]

        return summary, sas_id_to_id


def store_summary_file(path, summary):
    with open(path, 'w') as f_stream:
        json.dump(summary, f_stream)


def find_surl_and_size_per_sasid(engine, sasid):
    result_df = pandas.read_sql_query(QUERY_STR.format(sasid), engine)
    return result_df


class OutOfRange(Exception):
    pass


def from_ra_dec_to_xy(ra, dec, t_obs):
    sun_coord = np_array(list(map(lambda x: x.rad, sun_position(t_obs, False))))
    rotation_angle = precession_angle(t_obs)

    shift = sun_coord - np_array((ra, dec))

    rotation_matrix = np_array([[cos(rotation_angle), -sin(rotation_angle)],
                                [sin(rotation_angle), cos(rotation_angle)]])
    xx, yy = shift @ rotation_matrix * 3600
    if (xx ** 2 + yy ** 2) >= 3000:
        raise OutOfRange
    return xx, yy


def main():
    args = parse_args()
    summary, sas_id_to_id = read_summary_file(args.summary_file)
    credentials = read_user_credentials(DEFAULT_CONFIG_PATHS)
    engine = create_db_engine(**credentials)

    for sas_id, ids_to_process in sas_id_to_id.items():
        sas_id = int(sas_id)
        logger.info('fetching surl for sas_id %s', sas_id)
        table = find_surl_and_size_per_sasid(engine, sas_id)
        for id_to_process in ids_to_process:
            for time_stamp in summary[id_to_process]:
                fname = summary[id_to_process][time_stamp]['source']
                sub_table = table[table.filename == fname]

                if len(sub_table) > 0:
                    ra = float(sub_table.ra)
                    dec = float(sub_table.dec)
                    summary[id_to_process][time_stamp]['ra'] = ra
                    summary[id_to_process][time_stamp]['dec'] = dec

                    try:
                        x, y = from_ra_dec_to_xy(ra,
                                                 dec,
                                                 time_stamp)

                        summary[id_to_process][time_stamp]['x'], \
                        summary[id_to_process][time_stamp]['y'] = x, y
                        summary[id_to_process][time_stamp]['pointing_name'] = 'Sun'
                    except OutOfRange:
                        summary[id_to_process][time_stamp]['pointing_name'] = 'Calibrator'

    store_summary_file(args.summary_file, summary)


if __name__ == '__main__':
    main()
