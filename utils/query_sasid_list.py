#!/h5_to_fits/env python
from argparse import ArgumentParser
import pandas
import urllib.parse
from sqlalchemy import create_engine
from typing import Dict, Union, List, Tuple
import logging
import os.path
from configparser import ConfigParser

CONNECTION_STRING = 'oracle+cx_oracle://{user}:{password}@db.lofar.target.rug.nl/?service_name=db.lofar.target.rug.nl'
DEFAULT_CONFIG_PATHS = ['~/.config/solar_processing/config.ini', '~/.awe/Environment.cfg']


def parse_args():
    parser = ArgumentParser(description='Query the LTA to obtain the list of observation')
    parser.add_argument('sas_ids', help='List of SAS IDS', nargs='+')
    parser.add_argument('output_dir', help='Output directory')
    return parser.parse_args()


logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s', level=logging.DEBUG)

logger = logging.getLogger('Solar query LTA')

COLUMN_NAME = '(System) Coherent Stokes (BF data)'
QUERY_STR = '''
SELECT FO.OBJECT_ID AS OBJECT_ID,
       FO.FILESIZE AS FILESIZE,
       ST.OBS_ID as OBS_ID,
       FO."+PROJECT" AS PROJECT,
       STR.PRIMARY_URL AS PRIMARY_URL FROM AWOPER.FILEOBJECT FO JOIN
    AWOPER.STORAGETICKETRESOURCE STR ON FO.STORAGE_TICKET_RESOURCE=STR.OBJECT_ID JOIN
    AWOPER.STORAGETICKET ST ON ST.OBJECT_ID=STR.TICKET
       WHERE OBS_ID = '{}' AND STR.PRIMARY_URL LIKE '%S0%'
'''


def read_user_credentials(file_paths: Union[str, List[str]]) -> Dict:
    file_paths = list(map(os.path.expanduser, map(os.path.expandvars, file_paths)))

    parser = ConfigParser()
    parser.read(file_paths)
    credentials = {'user': parser['global']['database_user'], 'password': parser['global']['database_password']}
    return credentials


def create_db_engine(user: str, password: str):
    connection_string: str = CONNECTION_STRING.format(user=urllib.parse.quote_plus(user),
                                                      password=urllib.parse.quote_plus(password))

    return create_engine(connection_string)


def read_observation_list(path):
    data_frame = pandas.read_excel(path, skiprows=1)
    return data_frame[data_frame[COLUMN_NAME] == True]


def find_surl_and_size_per_sasid(engine, sasid):

    result_df = pandas.read_sql_query(QUERY_STR.format(sasid), engine)
    return result_df


def main():
    args = parse_args()
    sas_ids = args.sas_ids
    credentials = read_user_credentials(DEFAULT_CONFIG_PATHS)
    engine = create_db_engine(**credentials)
    for sas_id in sas_ids:
        sas_id = int(sas_id)
        logger.info('fetching surl for sas_id %s', sas_id)
        surl_path = os.path.join(args.output_dir, f'{sas_id}.surl')
        surl_path_all = os.path.join(args.output_dir, f'all.surl')
        os.makedirs(args.output_dir, exist_ok=True
                    )
        table = find_surl_and_size_per_sasid(engine, sas_id)
        with open(surl_path_all, 'a+') as f_stream:
            for p_url, f_size in zip(table['primary_url'], table['filesize']):
                f_stream.write(f'{f_size};{p_url}\n')

        with open(surl_path, 'w') as f_stream:
            for p_url, f_size in zip(table['primary_url'], table['filesize']):
                f_stream.write(f'{f_size};{p_url}\n')


if __name__ == '__main__':
    main()
