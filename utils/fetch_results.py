import json
import sys
from argparse import ArgumentParser
import requests
import os
import subprocess


class ATDBAPIError(Exception):
    pass


def parse_args():
    parser = ArgumentParser(description='Fetch data based on the workflow')
    parser.add_argument('--ldv_atdb_url', help='API url', default='https://sdc.astron.nl:5554/atdb')
    parser.add_argument('--workflow', help='workflow slug', default='solar_bf_compress_v01')
    parser.add_argument('--status', help='status from which copy the tasks', default='archived', nargs='+')
    parser.add_argument('--summary_file', help='summary file', default='summary.json')
    parser.add_argument('--use_container', help='container name', default='alquerque/ldv-services')
    parser.add_argument('output_directory', help='directory where to stored the downloaded data')
    return parser.parse_args()


session = requests.session()


def rest_get(url, parameters=None):
    response = session.get(url, params=parameters)
    if response.ok:
        return response.json()
    else:
        raise ATDBAPIError('%s , %s- %s' % (url, response.status_code, response.reason))


def rest_path(atdb_url, path):
    return '/'.join((atdb_url.rstrip('/'), path.lstrip('/')))


def fetch_list_of_processed_data(atdb_url, workflow_slug, statuses):
    workflow, *_ = filter(lambda x: x['workflow_uri'] == workflow_slug,
                          rest_get(rest_path(atdb_url, 'workflows'))['results'])

    response = rest_get('/'.join((atdb_url, 'tasks/')), {'workflow__id': workflow['id'], 'status__in': statuses})
    tasks = response['results']
    while response['next'] != None:
        response = rest_get(response['next'].replace('http://', 'https://'),
                            {'workflow': workflow['id'], 'status__in': statuses})
        tasks += response['results']

    return tasks


def from_task_to_unique_id(task):
    return '%s-%s' % (task['sas_id'], task['id'])


def compute_path(task, output_directory):
    task_id = task['id']

    main_group = task_id // 1000
    task_path = from_task_to_unique_id(task)
    path_name = os.path.join(str(main_group), task_path)
    full_path = os.path.join(output_directory, path_name)
    return full_path, path_name

import tempfile
def download_url_list(url_list, storage_path, container):
    HOME = os.path.expanduser('~')
    with tempfile.NamedTemporaryFile(mode='r+') as f_stream:
        for line in url_list:
            f_stream.write(f'{line}\n')
        f_stream.flush()

        cmd = f'udocker run -e X509_USER_PROXY=/globus/proxy.cert -v {HOME}/.globus:/globus -v {storage_path}:{storage_path} -v {f_stream.name}:{f_stream.name} {container} globus-url-copy -f {f_stream.name}'

        result = subprocess.run(cmd, capture_output=True, shell=True)
        try:
            result.check_returncode()
        except subprocess.CalledProcessError as e:
            print('An error occurred', e)
            print(result.stdout)
            print(result.stderr)


def make_download_strings(file_surl, storage_path):
    fname = file_surl.split('/')[-1]
    input_path = _surl_to_gftp(file_surl)
    output_path = os.path.join(storage_path, fname)
    return f'{input_path} file://{output_path}', output_path, fname


def download_task_output(task, output_directory, container=None):
    output_files = task['outputs']

    store_directory_path, path_name = compute_path(task, output_directory)
    os.makedirs(store_directory_path, exist_ok=True)

    file_to_download_list = []
    file_list = []
    for key, file_objects in output_files.items():
        for file_obj in file_objects[0]:

            url_string, output_path, fname = make_download_strings(file_obj['surl'], store_directory_path)

            file_list.append(f'{path_name}/{fname}')

            if os.path.exists(output_path):
                continue
            file_to_download_list.append(url_string)

    if file_to_download_list:
        download_url_list(file_to_download_list, store_directory_path, container=container)
    return file_list


def _surl_to_gftp(surl):
    return surl.replace('srm://srm.grid.sara.nl/', 'gsiftp://gridftp.grid.sara.nl/')


def store_summary(summary_obj, summary_file):
    with open(summary_file, 'w') as summary_file_stream:
        json.dump(summary_obj, summary_file_stream)


def order_fire_list(output_dir, file_list):

    json_files = filter(lambda x: x.endswith('json'), file_list)
    object = {}
    for json_file in json_files:
        with open(os.path.join(output_dir, json_file), 'r') as fin:
            time_chunk = json.load(fin)
            time_chunk['png'] = json_file.replace('.json','.png')
            time_chunk['fits'] = json_file.replace('.json','.fits')
            date = time_chunk['date']
            time = time_chunk['time']
            datetime = f'{date}T{time}'
            object[datetime] = time_chunk
    return object

def main():
    args = parse_args()
    tasks = fetch_list_of_processed_data(args.ldv_atdb_url, args.workflow, args.status)
    summary = {}

    for item_num, task in enumerate(tasks):
        print('processing task {:s} - {:d} of {:d} - {:.2f} % done'.format(from_task_to_unique_id(task),
                                                                           item_num + 1,
                                                                           len(tasks),
                                                                           (item_num + 1) / len(tasks) * 100))

        file_list = download_task_output(task, args.output_directory, container=args.use_container)
        product_summary = order_fire_list(args.output_directory, file_list)
        summary[from_task_to_unique_id(task)] = product_summary
    store_summary(summary, args.summary_file)


if __name__ == '__main__':
    main()
